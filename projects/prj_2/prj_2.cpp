#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

int main()
{
	Mat img1 = imread("../apple_256x256.png"); //Открываем картинку
	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
	compression_params.push_back(1);
	compression_params[1] = 95; //Компрессия 95%
	imwrite("../apple_95.jpeg", img1, compression_params); //Сохраняем картинку
	compression_params[1] = 65; //Компрессия 65%
	imwrite("../apple_65.jpeg", img1, compression_params); //Сохраняем картинку
	Mat img2 = imread("../apple_95.jpeg"); //Открываем картинки
	Mat img3 = imread("../apple_65.jpeg");
	Mat diffimage(Mat::zeros(512, 768, CV_8UC3)); //Визуализация поканального яркостного различия
	for (int i = 0; i < 256; i += 1)
		for (int j = 0; j < 256; j += 1)
		{
			//Значения яркости по каналам у 1й картинки
			int b1 = img1.at<cv::Vec3b>(i, j)[0];
			int g1 = img1.at<cv::Vec3b>(i, j)[1];
			int r1 = img1.at<cv::Vec3b>(i, j)[2];
			//Значения яркости по каналам у 2й картинки
			int b2 = img2.at<cv::Vec3b>(i, j)[0];
			int g2 = img2.at<cv::Vec3b>(i, j)[1];
			int r2 = img2.at<cv::Vec3b>(i, j)[2];
			//Значения яркости по каналам у 3й картинки
			int b3 = img3.at<cv::Vec3b>(i, j)[0];
			int g3 = img3.at<cv::Vec3b>(i, j)[1];
			int r3 = img3.at<cv::Vec3b>(i, j)[2];
			//Яркостные различия 1й и 2й картинки в голубом канале
			diffimage.at<cv::Vec3b>(i, j)[0] = abs(b1 - b2) * 20; //Увеличиваем яркость для наглядности
			diffimage.at<cv::Vec3b>(i, j)[1] = 0;
			diffimage.at<cv::Vec3b>(i, j)[2] = 0;
			//Яркостные различия 1й и 2й картинки в зеленом канале
			diffimage.at<cv::Vec3b>(i, j + 256)[0] = 0;
			diffimage.at<cv::Vec3b>(i, j + 256)[1] = abs(g1 - g2) * 20;
			diffimage.at<cv::Vec3b>(i, j + 256)[2] = 0;
			//Яркостные различия 1й и 2й картинки в красном канале
			diffimage.at<cv::Vec3b>(i, j + 512)[0] = 0;
			diffimage.at<cv::Vec3b>(i, j + 512)[1] = 0;
			diffimage.at<cv::Vec3b>(i, j + 512)[2] = abs(r1 - r2) * 20;
			//Яркостные различия 1й и 3й картинки в голубом канале
			diffimage.at<cv::Vec3b>(i + 256, j)[0] = abs(b1 - b2) * 20;
			diffimage.at<cv::Vec3b>(i + 256, j)[1] = 0;
			diffimage.at<cv::Vec3b>(i + 256, j)[2] = 0;
			//Яркостные различия 1й и 3й картинки в зеленом канале
			diffimage.at<cv::Vec3b>(i + 256, j + 256)[0] = 0;
			diffimage.at<cv::Vec3b>(i + 256, j + 256)[1] = abs(g1 - g2) * 20;
			diffimage.at<cv::Vec3b>(i + 256, j + 256)[2] = 0;
			//Яркостные различия 1й и 3й картинки в красном канале
			diffimage.at<cv::Vec3b>(i + 256, j + 512)[0] = 0;
			diffimage.at<cv::Vec3b>(i + 256, j + 512)[1] = 0;
			diffimage.at<cv::Vec3b>(i + 256, j + 512)[2] = abs(r1 - r2) * 20;
		}
	imshow("Поканальные яркостные различия", diffimage);
	waitKey(0);
	return 0;
}