#include <opencv2/opencv.hpp>
#include <cstdio>
#include <iostream>
#include <cmath>

using namespace cv;

int main(int argc, char* argv[])
{
	Mat image(Mat::zeros(400, 600, CV_8UC1));

	for (int i = 0; i < 200; i += 1)
		for (int j = 0; j < 200; j += 1)
		{
			image.at<uchar>(i, j) = 127;
			image.at<uchar>(i, j + 400) = 255;
			image.at<uchar>(i + 200, j) = 255;
			image.at<uchar>(i + 200, j + 200) = 127;

		}

	circle(image, Point(100, 100), 50, 255, -1, 8, 0);
	circle(image, Point(300, 100), 50, 127, -1, 8, 0);
	circle(image, Point(500, 100), 50, 0, -1, 8, 0);

	circle(image, Point(100, 300), 50, 127, -1, 8, 0);
	circle(image, Point(300, 300), 50, 0, -1, 8, 0);
	circle(image, Point(500, 300), 50, 255, -1, 8, 0);

    Mat kernel(2, 2, CV_32F);
	Mat kernel1(2, 2, CV_32F);

	kernel.at<float>(0, 0) = 1;
	kernel.at<float>(1, 0) = 0;
	kernel.at<float>(0, 1) = 0;
	kernel.at<float>(1, 1) = -1;

	kernel1.at<float>(0, 0) = 0;
	kernel1.at<float>(1, 0) = 1;
	kernel1.at<float>(0, 1) = -1;
	kernel1.at<float>(1, 1) = 0;

	Point anchor = Point(-1, -1);

	double delta = 0;
   
	Mat dst; // результат
	Mat dst1; // результат
	Mat dst2; // результат
	
    // накладываем фильтр
	filter2D(image, dst, CV_32F, kernel, anchor, delta);
	filter2D(image, dst1, CV_32F, kernel1, anchor, delta);
	pow(dst.mul(dst) + dst1.mul(dst1), 0.5, dst2);
	dst = ((dst + 255) / 2);
	dst1 = ((dst1 + 255) / 2);
	
	dst.convertTo(dst, CV_8UC1);
	dst1.convertTo(dst1, CV_8UC1);
	dst2.convertTo(dst2, CV_8UC1);

    // показываем картинку
	imshow("pikcha", image);
	imshow("Resault", dst);
	imshow("Resault1", dst1);
	imshow("Resault2", dst2);

	waitKey(0);
	return 0;
}
