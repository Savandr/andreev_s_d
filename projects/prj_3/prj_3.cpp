#include <opencv2/opencv.hpp>
using namespace cv;

int main()
{
    //WARNING! ОСТОРОЖНО! КОЛХОЗ! ATTENTION!
    Mat img(Mat::zeros(256, 256, CV_8UC1));
    img = imread("../cross_0256x0256.png", 0); //Читаем картинку (0 - ч/б, 1 - цветная)

    int hist[256]; //создаем массив гистограммы
    for (int i = 0; i < 256; i++) //забиваем его нулями (не на питоне ж пишем, так что канон)
    {
        hist[i] = 0;
    }
    for (int i_strok = 0; i_strok < 256; i_strok++)     //фигачим по пикселям изображения прибавляя еденичку
        for (int j_stolb = 0; j_stolb < 256; j_stolb++) //к элементу массива соответсвующему по индексу
        {                                               //насыщенонсти данного пикселя
            hist[img.at<uchar>(i_strok, j_stolb)] += 1;
        }

    Mat img2(Mat::zeros(256, 768, CV_8UC1));            //создаем картинку, куда будем рисовать гистограмму

    int mashtab_k = 0;                  //высчитываем коэффициент маштабирования гистограммы
    for (int i = 0; i < 256; i++)       //что бы она потом не вылазила за окно вывода 
    {
        if (hist[i] > mashtab_k)
            mashtab_k = hist[i];
    }
    mashtab_k /= 256;

    for (int i_strok = 255; i_strok >= 0; i_strok--)  // отрисовываем гистограмму
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (hist[j_stolb] > 0)
            {
                img2.at<uchar>(i_strok, j_stolb*3) = 255;       //для наглядности делаем столбцы шире
                img2.at<uchar>(i_strok, j_stolb*3 + 1) = 255;   //окрашивая по 3 пикселя
                img2.at<uchar>(i_strok, j_stolb*3 + 2) = 255;
                hist[j_stolb] -= mashtab_k;                     //маштабируем ее, относительно самого большого значения
            }
        }
    for (int i_strok = 255; i_strok >= 0; i_strok--)            // а теперь меням черый и белый местами
        for (int j_stolb = 0; j_stolb < 768; j_stolb++)         // потому что пол часа гугления не обяснили как
        {                                                       // просто создать белую картинку
            if (img2.at<uchar>(i_strok, j_stolb) == 255)
            {
                img2.at<uchar>(i_strok, j_stolb) = 0;
            }
            else
            {
                img2.at<uchar>(i_strok, j_stolb) = 255;
            }
        }

    imshow("Исходная картинка", img);     //выводим исходную картинку
    imshow("Гистограмма для исходной картинки", img2); //выводим гистограмму
    
    //ЧАСТЬ 2============================================================================================   
    // Построить график функции
    
    Mat imgG(Mat::zeros(257, 257, CV_8UC1)); //окно для вывода графика
    int mass[256]; //массив для значений функции
    for (int i = 0; i < 256; i++) //снова обнуляем
    {
        mass[i] = 0;
    }
    int stlb = 0;

    //int look_up_table[256];
    for (int i = 0; i < 256; i++) //забиваем массив значениями функции
    {
        if (i <= 128)
        {
            mass[i] =  i + ((abs(i - 128)) / 2);
        }
        else
        {
            mass[i] = i - ((abs(i - 128)) / 2);
        }
    }

    for (int j_stolb = 0; j_stolb < 256; j_stolb++) // отрисовываем графиик
    {
        imgG.at<uchar>((256 - mass[j_stolb]), j_stolb) = 255;
    }
            
    imshow("График функции", imgG); //вывод графика на экран

    //-----------------------------------------------------------------------------------------------------
    //Применение функции яркостного преобразования

    Mat img3 = imread("../cross_0256x0256.png", 0); //задаем картинку
    
    for (int i_strok = 0; i_strok < 256; i_strok++) //применяем look up table 
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            img3.at<uchar>(i_strok, j_stolb) = mass[img3.at<uchar>(i_strok, j_stolb)];
        }      
    
    /*
    for (int i_strok = 0; i_strok < 256; i_strok++) //применяем функцию
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (img3.at<uchar>(i_strok, j_stolb) > 128) // если яркость больше половины 128, то понижаем в 2 раза, если меньше, повышаем
            {           
                img3.at<uchar>(i_strok, j_stolb) -= ((abs((img3.at<uchar>(i_strok, j_stolb)) - 128)) / 2);
            }
            else
            {
                img3.at<uchar>(i_strok, j_stolb) += ((abs((img3.at<uchar>(i_strok, j_stolb)) - 128)) / 2);
            }
        }
    */

    for (int i = 0; i < 256; i++)  // вывод look up table
    {
        std::cout << i << "  " ;
    }
    std::cout << "/n";
    for (int i = 0; i < 256; i++)
    {
        std::cout << mass[i] << "  ";
    }


    int hist2[256]; // дальше все так-же как в первой части
    for (int i = 0; i < 256; i++)
    {
        hist2[i] = 0;
    }
    for (int i_strok = 0; i_strok < 256; i_strok++)
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            hist2[img3.at<uchar>(i_strok, j_stolb)] += 1;
        }

    Mat img4(Mat::zeros(256, 768, CV_8UC1));

    int mashtab_k2 = 0;
    for (int i = 0; i < 256; i++)
    {
        if (hist2[i] > mashtab_k2)
            mashtab_k2 = hist2[i];
    }
    mashtab_k2 /= 256;

    for (int i_strok = 255; i_strok >= 0; i_strok--)
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (hist2[j_stolb] > 0)
            {
                img4.at<uchar>(i_strok, j_stolb * 3) = 255;
                img4.at<uchar>(i_strok, j_stolb * 3 + 1) = 255;
                img4.at<uchar>(i_strok, j_stolb * 3 + 2) = 255;
                hist2[j_stolb] -= mashtab_k2;
            }
        }
    for (int i_strok = 255; i_strok >= 0; i_strok--)
        for (int j_stolb = 0; j_stolb < 768; j_stolb++)
        {
            if (img4.at<uchar>(i_strok, j_stolb) == 255)
            {
                img4.at<uchar>(i_strok, j_stolb) = 0;
            }
            else
            {
                img4.at<uchar>(i_strok, j_stolb) = 255;
            }
        }
    imshow("Картинка с яркостным преобразованием", img3);
    imshow("Гистограмма картинки с яркостным преобразованием", img4);
 
    //===================================================================================================

    // Начинаем баловаться CLAHE'ми
    Mat m1 = imread("../cross_0256x0256.png", IMREAD_GRAYSCALE); //input image
    Mat m2 = imread("../cross_0256x0256.png", IMREAD_GRAYSCALE); //input image
    Mat m3 = imread("../cross_0256x0256.png", IMREAD_GRAYSCALE); //input image

    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(2);

    Mat dst1;
    clahe->apply(m1, dst1);
    imshow("CLAHE1", dst1); //Выводим картинку с clahe1

    int hist_clahe_1[256]; //создаем массив гистограммы для clahe1
    for (int i = 0; i < 256; i++) //забиваем его нулями (не на питоне ж пишем, так что канон)
    {
        hist_clahe_1[i] = 0;
    }
    for (int i_strok = 0; i_strok < 256; i_strok++)     //фигачим по пикселям изображения прибавляя еденичку
        for (int j_stolb = 0; j_stolb < 256; j_stolb++) //к элементу массива соответсвующему по индексу
        {                                               //насыщенонсти данного пикселя
            hist_clahe_1[dst1.at<uchar>(i_strok, j_stolb)] += 1;
        }

    Mat hist_clahe_1_img(Mat::zeros(256, 768, CV_8UC1)); //создаем картинку, куда будем рисовать гистограмму

    int mashtab_k_clh_1 = 0;                //высчитываем коэффициент маштабирования гистограммы
    for (int i = 0; i < 256; i++)           //что бы она потом не вылазила за окно вывода 
    {
        if (hist_clahe_1[i] > mashtab_k_clh_1)
            mashtab_k_clh_1 = hist_clahe_1[i];
    }
    mashtab_k_clh_1 /= 256;

    for (int i_strok = 255; i_strok >= 0; i_strok--)  // отрисовываем гистограмму
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (hist_clahe_1[j_stolb] > 0)
            {
                hist_clahe_1_img.at<uchar>(i_strok, j_stolb * 3) = 255;       //для наглядности делаем столбцы шире
                hist_clahe_1_img.at<uchar>(i_strok, j_stolb * 3 + 1) = 255;   //окрашивая по 3 пикселя
                hist_clahe_1_img.at<uchar>(i_strok, j_stolb * 3 + 2) = 255;
                hist_clahe_1[j_stolb] -= mashtab_k_clh_1; //маштабируем ее, относительно самого большого значения
            }
        }
    for (int i_strok = 255; i_strok >= 0; i_strok--)            // а теперь меням черый и белый местами
        for (int j_stolb = 0; j_stolb < 768; j_stolb++)         // потому что пол часа гугления не обяснили как
        {                                                       // просто создать белую картинку
            if (hist_clahe_1_img.at<uchar>(i_strok, j_stolb) == 255)
            {
                hist_clahe_1_img.at<uchar>(i_strok, j_stolb) = 0;
            }
            else
            {
                hist_clahe_1_img.at<uchar>(i_strok, j_stolb) = 255;
            }
        }

    imshow("Гистограмма Clahe1", hist_clahe_1_img); //выводим гистограмму

    //===================================================================================================

    clahe->setClipLimit(4);

    Mat dst2;
    clahe->apply(m2, dst2);
    imshow("CLAHE2", dst2); //Выводим картинку с clahe2

    int hist_clahe_2[256]; //создаем массив гистограммы для clahe2
    for (int i = 0; i < 256; i++) //забиваем его нулями (не на питоне ж пишем, так что канон)
    {
        hist_clahe_2[i] = 0;
    }
    for (int i_strok = 0; i_strok < 256; i_strok++)     //фигачим по пикселям изображения прибавляя еденичку
        for (int j_stolb = 0; j_stolb < 256; j_stolb++) //к элементу массива соответсвующему по индексу
        {                                               //насыщенонсти данного пикселя
            hist_clahe_2[dst2.at<uchar>(i_strok, j_stolb)] += 1;
        }

    Mat hist_clahe_2_img(Mat::zeros(256, 768, CV_8UC1)); //создаем картинку, куда будем рисовать гистограмму

    int mashtab_k_clh_2 = 0;                //высчитываем коэффициент маштабирования гистограммы
    for (int i = 0; i < 256; i++)           //что бы она потом не вылазила за окно вывода 
    {
        if (hist_clahe_2[i] > mashtab_k_clh_2)
            mashtab_k_clh_2 = hist_clahe_2[i];
    }
    mashtab_k_clh_2 /= 256;

    for (int i_strok = 255; i_strok >= 0; i_strok--)  // отрисовываем гистограмму
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (hist_clahe_2[j_stolb] > 0)
            {
                hist_clahe_2_img.at<uchar>(i_strok, j_stolb * 3) = 255;       //для наглядности делаем столбцы шире
                hist_clahe_2_img.at<uchar>(i_strok, j_stolb * 3 + 1) = 255;   //окрашивая по 3 пикселя
                hist_clahe_2_img.at<uchar>(i_strok, j_stolb * 3 + 2) = 255;
                hist_clahe_2[j_stolb] -= mashtab_k_clh_2; //маштабируем ее, относительно самого большого значения
            }
        }
    for (int i_strok = 255; i_strok >= 0; i_strok--)            // а теперь меням черый и белый местами
        for (int j_stolb = 0; j_stolb < 768; j_stolb++)         // потому что пол часа гугления не обяснили как
        {                                                       // просто создать белую картинку
            if (hist_clahe_2_img.at<uchar>(i_strok, j_stolb) == 255)
            {
                hist_clahe_2_img.at<uchar>(i_strok, j_stolb) = 0;
            }
            else
            {
                hist_clahe_2_img.at<uchar>(i_strok, j_stolb) = 255;
            }
        }

    imshow("Гистограмма Clahe2", hist_clahe_2_img); //выводим гистограмму

    //===================================================================================================

    clahe->setClipLimit(6);

    Mat dst3;
    clahe->apply(m3, dst3);
    imshow("CLAHE3", dst3); //Выводим картинку с clahe3

    int hist_clahe_3[256]; //создаем массив гистограммы для clahe3
    for (int i = 0; i < 256; i++) //забиваем его нулями (не на питоне ж пишем, так что канон)
    {
        hist_clahe_3[i] = 0;
    }
    for (int i_strok = 0; i_strok < 256; i_strok++)     //фигачим по пикселям изображения прибавляя еденичку
        for (int j_stolb = 0; j_stolb < 256; j_stolb++) //к элементу массива соответсвующему по индексу
        {                                               //насыщенонсти данного пикселя
            hist_clahe_3[dst3.at<uchar>(i_strok, j_stolb)] += 1;
        }

    Mat hist_clahe_3_img(Mat::zeros(256, 768, CV_8UC1)); //создаем картинку, куда будем рисовать гистограмму

    int mashtab_k_clh_3 = 0;                //высчитываем коэффициент маштабирования гистограммы
    for (int i = 0; i < 256; i++)           //что бы она потом не вылазила за окно вывода 
    {
        if (hist_clahe_3[i] > mashtab_k_clh_3)
            mashtab_k_clh_3 = hist_clahe_3[i];
    }
    mashtab_k_clh_3 /= 256;

    for (int i_strok = 255; i_strok >= 0; i_strok--)  // отрисовываем гистограмму
        for (int j_stolb = 0; j_stolb < 256; j_stolb++)
        {
            if (hist_clahe_3[j_stolb] > 0)
            {
                hist_clahe_3_img.at<uchar>(i_strok, j_stolb * 3) = 255;       //для наглядности делаем столбцы шире
                hist_clahe_3_img.at<uchar>(i_strok, j_stolb * 3 + 1) = 255;   //окрашивая по 3 пикселя
                hist_clahe_3_img.at<uchar>(i_strok, j_stolb * 3 + 2) = 255;
                hist_clahe_3[j_stolb] -= mashtab_k_clh_3; //маштабируем ее, относительно самого большого значения
            }
        }
    for (int i_strok = 255; i_strok >= 0; i_strok--)            // а теперь меням черый и белый местами
        for (int j_stolb = 0; j_stolb < 768; j_stolb++)         // потому что пол часа гугления не обяснили как
        {                                                       // просто создать белую картинку
            if (hist_clahe_3_img.at<uchar>(i_strok, j_stolb) == 255)
            {
                hist_clahe_3_img.at<uchar>(i_strok, j_stolb) = 0;
            }
            else
            {
                hist_clahe_3_img.at<uchar>(i_strok, j_stolb) = 255;
            }
        }

    imshow("Гистограмма Clahe3", hist_clahe_3_img); //выводим гистограмму

    //===================================================================================================
    Mat img_for_threshold = imread("../cross_0256x0256.png", IMREAD_GRAYSCALE); //input image
    Mat res_threshold; //Результат бинаризации
    int necessary_threshold = 128; //Задаём порог
    threshold(img_for_threshold, res_threshold, necessary_threshold, 255, THRESH_BINARY); //Применяем функцию
    imshow("Пороговая бинаризация", res_threshold); //Выводим результат
    //===================================================================================================
    Mat img_for_local_binarization = imread("../cross_0256x0256.png", IMREAD_GRAYSCALE); //input image
    Mat res_local_binarization; //Результат локальной бинаризации
    adaptiveThreshold(img_for_local_binarization, res_local_binarization, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 3, 2); //Применяем функцию
    imshow("Локальная бинаризация", res_local_binarization); //Выводим результат
    //===================================================================================================
    Mat res_morphology; //Результат морфологической операции
    Mat kernel = Mat(6, 6, CV_32F); //Матрица ядра
    morphologyEx(res_threshold, res_morphology, MORPH_TOPHAT, kernel); //Применяем функцию
    imshow("Морфологический фильтр", res_morphology); //Выводим результат
    //===================================================================================================
    /*Mat img_for_morphology = imread("C:/Users/savan/Desktop/andreev_s_d/cross_0256x0256.png", IMREAD_GRAYSCALE); //input image
    Mat res_morphology; //Результат морфологической операции
    Mat kernel = Mat(6, 6, CV_32F); //Матрица ядра
    morphologyEx(img_for_morphology, res_morphology, MORPH_TOPHAT, kernel); //Применяем функцию
    imshow("Морфологический фильтр", res_morphology); //Выводим результат
    Mat res_morphology_threshold; //Результат бинаризации
    int necessary_threshold_2 = 15; //Задаём порог
    threshold(res_morphology, res_morphology_threshold, necessary_threshold_2, 255, THRESH_BINARY); //Применяем функцию
    //adaptiveThreshold(res_morphology, res_morphology_threshold, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 3, 2); //Применяем функцию
    imshow("Бинаризация после фильтра", res_morphology_threshold); //Выводим результат */
    //===================================================================================================
    Mat alpha_image;
    double alpha = 0.7;
    addWeighted(img_for_threshold, alpha, res_morphology, 1 - alpha, 0.0, alpha_image);
    imshow("alpha_image", alpha_image);
    //===================================================================================================
   
    waitKey(0);
    return 0;

    //Это остатки неудачных попыток сделать меньше колхоза.
    //void bitwise_not(InputArray src, OutputArray dst, InputArray mask = noArray());
    //#define w 200
    //void rectangle(Mat & img2, Point i_stolb, Point j_strok, const Scalar & green, int thickness = 1, int lineType = 8, int shift = 0);
    //Point pt1 = Point(10, 10);
    //Point pt2 = Point(100, 100);
    //Scalar(150, 0, 0);
    //rectangle(img2,
    //    Point(0, 7 * w / 8),
    //    Point(w, w),
    //    Scalar(0, 255, 255),
    //    FILLED,
    //    LINE_8);

    //void rectangle(Mat & img2, Point pt1, Point pt2, const Scalar & color, int thickness = 1, int lineType = 8, int shift = 0);

    //for (int i_stolb = 0; i_stolb < 256; i_stolb++)
    //   for (int j_strok = 0; j_strok < 256; j_strok++)
    //    {
    //        void rectangle(Mat & img2, Point i_stolb, Point j_strok, const Scalar & blue, int thickness = 1, int lineType = 8, int shift = 0);
    //    }

    //for (int i_stolb = 0; i_stolb < 256; i_stolb++)
    //    for (int j_strok = 0; j_strok < 256; j_strok++)
    //    {
    //        void rectangle(Mat & img3, Point pt1, Point pt2, const Scalar & color, int thickness = 1, int lineType = 8, int shift = 0);
    //    }
}